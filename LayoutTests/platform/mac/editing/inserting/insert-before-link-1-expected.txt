EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 3 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 0 of DIV > BODY > HTML > #document to 0 of DIV > BODY > HTML > #document toDOMRange:range from 25 of #text > A > DIV > BODY > HTML > #document to 25 of #text > A > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 5 of #text > DIV > BODY > HTML > #document to 5 of #text > DIV > BODY > HTML > #document toDOMRange:range from 17 of #text > DIV > BODY > HTML > #document to 17 of #text > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 12 of #text > A > DIV > BODY > HTML > #document to 12 of #text > A > DIV > BODY > HTML > #document toDOMRange:range from 17 of #text > DIV > BODY > HTML > #document to 17 of #text > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x54
        RenderText {#text} at (0,0) size 782x54
          text run at (0,0) width 360: "This tests insertion before/after links in certain situations. "
          text run at (360,0) width 406: "Inserting before a link should always put the text outside the link"
          text run at (0,18) width 352: "unless insertion is happening at the start of a paragraph. "
          text run at (352,18) width 430: "Inserting after should always insert inside the link, unless insertion is"
          text run at (0,36) width 241: "happening at the end of the document."
      RenderBlock {DIV} at (0,70) size 784x18
        RenderInline {A} at (0,0) size 179x18 [color=#0000EE]
          RenderText {#text} at (0,0) size 179x18
            text run at (0,0) width 179: "This text should be in a link."
        RenderText {#text} at (179,0) size 110x18
          text run at (179,0) width 110: " This should not. "
        RenderInline {A} at (0,0) size 78x18 [color=#0000EE]
          RenderText {#text} at (289,0) size 78x18
            text run at (289,0) width 78: "This should."
        RenderText {#text} at (367,0) size 106x18
          text run at (367,0) width 106: " This should not."
caret: position 17 of child 3 {#text} of child 2 {DIV} of child 0 {BODY} of child 0 {HTML} of document
